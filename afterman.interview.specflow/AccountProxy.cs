﻿using System;

namespace afterman.interview.specflow
{
    public class AccountProxy
    {
        private bool _isAuthenticated;

        private readonly UserRepo _userRepo = new UserRepo();

        public bool Authenticate(string username, string password)
        {
            var user = _userRepo.Get(username);
            if (user == null) return false;

            _isAuthenticated = username.Equals(user.Username, StringComparison.InvariantCultureIgnoreCase)
                && password.Trim().Equals(user.Password);

            return _isAuthenticated;
        }

        public string GetEmail(string username)
        {
            if (!_isAuthenticated) return "authentication required";

            var user = _userRepo.Get(username);
            return user == null ? "user not found" : user.Email;
        }

        public bool UpdateEmail(string username, string newEmail)
        {
            if (!_isAuthenticated) return false;

            var user = _userRepo.Get(username);
            if (user == null) return false;

            user.Email = newEmail;

            return true;
        }
    }
}
