﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace afterman.interview.specflow
{
    internal class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }

    internal class UserRepo
    {
        private readonly List<User> _users = new List<User>
        {
            new User
            {
                Username = "user1",
                Password = "password1",
                Email = "user1@users.com"
            },
            new User
            {
                Username = "user2",
                Password = "password2",
                Email = "user2@users.com"
            },
            new User
            {
                Username = "user3",
                Password = "password3",
                Email = "user3@users.com"
            }
        };

        public User Get(string username)
        {
            return _users.FirstOrDefault(u => u.Username.Equals(username.Trim(), StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
